#!/bin/bash

# Load env vars
if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi

# Docker compose exec function
RUN () {
  docker-compose exec -T php sh -c "$1"
}

# Provision
docker-compose up -d --remove-orphans

# Run composer install
RUN "robo job:build"

# Build theme & clean
RUN "cd web/themes/custom/bycnit_sass && npm install && ./node_modules/.bin/gulp && rm -rf node_modules"

# Set source owner
RUN "chown -R 1000:1000 /var/www/html"

# Remove all container
docker-compose down