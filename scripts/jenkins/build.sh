#!/bin/bash

# Load env vars
if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
fi

# Docker compose exec function
RUN () {
  docker-compose exec -T php sh -c "$1"
}

# Provision
docker-compose up -d --remove-orphans

# Run composer install
RUN "robo job:build"

# Build theme & clean
RUN "cd web/themes/custom/bycnit_sass && npm install && ./node_modules/.bin/gulp && rm -rf node_modules"

# Install Drupal
RUN "./vendor/bin/drush si --db-url=$DB_URL --account-name=$ADMIN_NAME --account-mail=$ADMIN_MAIL --account-pass=$ADMIN_PW -y"

# Set source owner
RUN "chown -R www-data:www-data ./src/web/sites/default/files"

# Remove all container
docker-compose down